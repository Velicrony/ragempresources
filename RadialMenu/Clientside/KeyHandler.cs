﻿using System;
using System.Collections.Generic;
using System.Linq;
using RAGE;
using RAGE.Elements;

namespace RadialMenuClientside
{
    public class KeyHandler : Events.Script
    {
        private readonly Dictionary<ConsoleKey, bool> _keyList = new Dictionary<ConsoleKey, bool>();
        private bool _isChatActive = false;

        private delegate void EventKeyDelegate(ConsoleKey key);
        private EventKeyDelegate KeyDownEvent { get; set; }
        private EventKeyDelegate KeyUpEvent { get; set; }

        public KeyHandler()
        {
            _keyList.Add(ConsoleKey.E, false);
            _keyList.Add(ConsoleKey.T, false);
            _keyList.Add(ConsoleKey.Enter, false);

            Events.Tick += OnUpdate;

            KeyDownEvent += OnKeyPressed;
            KeyUpEvent += OnKeyReleased;
        }

        private void OnKeyPressed(ConsoleKey key)
        {
            if (CefHelper.IsCefBrowserOpen())
            {
                if (CefHelper.IsSpecificCefBrowserVisible("package://cs_packages/RadialMenu/resources/radial-menu.html") && key == ConsoleKey.E)
                    Events.CallLocal("destroyRadialMenu");

                return;
            }

            if (key == ConsoleKey.T && !_isChatActive)
                _isChatActive = true;

            if (_isChatActive && key == ConsoleKey.Enter)
                _isChatActive = false;

            if (_isChatActive)
                return;

            if (key == ConsoleKey.E && !Player.LocalPlayer.IsInAnyVehicle(false))
                Events.CallLocal("eKeyPressed");
        }

        private void OnKeyReleased(ConsoleKey key)
        {
            if (CefHelper.IsCefBrowserOpen() || _isChatActive)
                return;
        }

        private void OnUpdate(List<Events.TickNametagData> nametags)
        {
            foreach (KeyValuePair<ConsoleKey, bool> key in _keyList.ToList())
            {
                if (_keyList[key.Key] && !Input.IsDown((int)key.Key))
                {
                    KeyUpEvent?.Invoke(key.Key);
                    _keyList[key.Key] = false;
                }
                else if (!_keyList[key.Key] && Input.IsDown((int)key.Key))
                {
                    KeyDownEvent?.Invoke(key.Key);
                    _keyList[key.Key] = true;
                }
            }
        }
    }
}
