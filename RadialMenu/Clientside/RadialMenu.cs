﻿using RAGE;

namespace RadialMenuClientside
{
    public class RadialMenu : Events.Script
    {
        private CefObject _cefRadialMenu = null;

        public RadialMenu()
        {
            Events.Add("eKeyPressed", EKeyPressed);
            Events.Add("showRadialMenu", ShowRadialMenu);
            Events.Add("destroyRadialMenu", DestroyRadialMenu);
            Events.Add("callRadialMenuServerFunction", CallRadialMenuServerFunction);
        }

        private void EKeyPressed(object[] args)
        {
            Events.CallRemote("showRadialMenu");
        }

        private void ShowRadialMenu(object[] args)
        {
            if (_cefRadialMenu != null)
            {
                CefHelper.DestroyCefBrowser(_cefRadialMenu);
                _cefRadialMenu = null;
            }

            string radialMenuItems = args[0].ToString();

            _cefRadialMenu = CefHelper.CreateCefBrowser("package://cs_packages/RadialMenu/resources/radial-menu.html");
            _cefRadialMenu.Execute($"addMenuItems(\"{radialMenuItems}\")");
        }

        private void DestroyRadialMenu(object[] args)
        {
            if (_cefRadialMenu != null)
            {
                CefHelper.DestroyCefBrowser(_cefRadialMenu);
                _cefRadialMenu = null;
            }
        }

        private void CallRadialMenuServerFunction(object[] args)
        {
            if (_cefRadialMenu != null)
            {
                CefHelper.DestroyCefBrowser(_cefRadialMenu);
                _cefRadialMenu = null;
            }

            Events.CallRemote(args[0].ToString());
        }
    }
}
