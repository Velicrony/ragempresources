﻿using GTANetworkAPI;

namespace RadialMenuServer
{
    public class RadialMenuHandler : Script
    {
        [RemoteEvent("showRadialMenu")]
        public void ShowRadialMenu(Client client, object[] arguments)
        {
            if (client.IsInVehicle)
                return;

            //DisplayName:Icon:RemoteEvent
            client.TriggerEvent("showRadialMenu", "Test2:user:Test;Test3:home:Test2;Test:trash:Test3");
        }

        [RemoteEvent("Test")]
        public void Test(Client client, object[] arguments)
        {
            client.SendNotification("Test");
        }

        [RemoteEvent("Test2")]
        public void Test2(Client client, object[] arguments)
        {
            client.SendNotification("Test2");
        }

        [RemoteEvent("Test3")]
        public void Test3(Client client, object[] arguments)
        {
            client.SendNotification("Test3");
        }
    }
}
