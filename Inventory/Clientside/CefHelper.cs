﻿using System;
using System.Collections.Generic;
using System.Linq;
using RAGE;
using RAGE.Ui;

namespace InventoryClientside
{
    public static class CefHelper
    {
        private static readonly List<CefObject> _cefObjects = new List<CefObject>();

        public static CefObject CreateCefBrowser(string path, int offsetX = 0, int offsetY = 0, bool isHud = false, Action destroyAction = null)
        {
            CefObject cefObject = new CefObject(path, offsetX, offsetY, isHud, destroyAction);
            cefObject.Show();

            _cefObjects.Add(cefObject);

            return cefObject;
        }

        public static void DestroyCefBrowser(CefObject cef)
        {
            cef.Destroy();

            _cefObjects.Remove(cef);
        }

        public static bool IsCefBrowserOpen()
        {
            return _cefObjects.Any(x => x.IsBrowserVisible());
        }

        public static bool IsSpecificCefBrowserVisible(string path)
        {
            return _cefObjects.Any(x => x.GetPath() == path && x.IsBrowserVisible());
        }

        public static List<CefObject> GetActiveCefs()
        {
            return _cefObjects;
        }

        public static CefObject GetSpecificCefBrowser(string path)
        {
            return _cefObjects.FirstOrDefault(x => x.GetPath() == path);
        }
    }

    public class CefObject
    {
        public string Path { get; private set; }
        public bool IsOpen { get; private set; }
        public int OffsetX { get; private set; }
        public int OffsetY { get; private set; }
        public bool IsHud { get; private set; }
        public bool IsActive { get; private set; }
        public Action DestroyAction { get; private set; }

        private HtmlWindow _htmlWindow;

        public CefObject(string path, int offsetX, int offsetY, bool isHud, Action destroyAction)
        {
            Path = path;
            IsOpen = false;
            OffsetX = offsetX;
            OffsetY = offsetY;
            IsHud = isHud;
            IsActive = false;
            DestroyAction = destroyAction;
        }

        public void Show()
        {
            if (!IsOpen)
            {
                IsOpen = true;

                _htmlWindow = new HtmlWindow(Path);
                _htmlWindow.Active = true;

                if (!IsHud)
                {
                    Cursor.Visible = true;
                    Chat.Show(false);
                }
            }
        }

        public void Destroy()
        {
            IsOpen = false;
            _htmlWindow.Destroy();

            DestroyAction?.Invoke();

            if (!IsHud)
            {
                Chat.Show(true);
                Cursor.Visible = false;
            }
        }

        public void Execute(string code)
        {
            _htmlWindow.ExecuteJs(code);
        }

        public HtmlWindow GetBrowser()
        {
            return _htmlWindow;
        }

        public void SetActiveState(bool isActive)
        {
            IsActive = isActive;
        }

        public bool IsBrowserVisible()
        {
            if (!IsHud)
                return true;

            return IsActive;

        }

        public string GetPath()
        {
            return Path;
        }
    }
}
