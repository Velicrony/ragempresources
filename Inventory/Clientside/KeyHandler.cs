﻿using System;
using System.Collections.Generic;
using System.Linq;
using RAGE;

namespace InventoryClientside
{
    public class KeyHandler : Events.Script
    {
        private readonly Dictionary<ConsoleKey, bool> _keyList = new Dictionary<ConsoleKey, bool>();
        private bool _isChatActive = false;

        private delegate void EventKeyDelegate(ConsoleKey key);
        private EventKeyDelegate KeyDownEvent { get; set; }
        private EventKeyDelegate KeyUpEvent { get; set; }

        public KeyHandler()
        {
            _keyList.Add(ConsoleKey.I, false);
            _keyList.Add(ConsoleKey.T, false);
            _keyList.Add(ConsoleKey.Enter, false);

            Events.Tick += OnUpdate;

            KeyDownEvent += OnKeyPressed;
            KeyUpEvent += OnKeyReleased;
        }

        private void OnKeyPressed(ConsoleKey key)
        {
            if (CefHelper.IsCefBrowserOpen())
            {
                if (CefHelper.IsSpecificCefBrowserVisible("package://cs_packages/Inventory/resources/inventory.html") && key == ConsoleKey.I)
                    Events.CallLocal("iKeyPressed");

                return;
            }

            if (key == ConsoleKey.T && !_isChatActive)
                _isChatActive = true;

            if (_isChatActive && key == ConsoleKey.Enter)
                _isChatActive = false;

            if (_isChatActive)
                return;

            if (key == ConsoleKey.I)
                Events.CallLocal("iKeyPressed");
        }

        private void OnKeyReleased(ConsoleKey key)
        {
            if (CefHelper.IsCefBrowserOpen() || _isChatActive)
                return;
        }

        private void OnUpdate(List<Events.TickNametagData> nametags)
        {
            foreach (KeyValuePair<ConsoleKey, bool> key in _keyList.ToList())
            {
                if (_keyList[key.Key] && !Input.IsDown((int)key.Key))
                {
                    KeyUpEvent?.Invoke(key.Key);
                    _keyList[key.Key] = false;
                }
                else if (!_keyList[key.Key] && Input.IsDown((int)key.Key))
                {
                    KeyDownEvent?.Invoke(key.Key);
                    _keyList[key.Key] = true;
                }
            }
        }
    }
}
