﻿using GTANetworkAPI;

namespace InventoryServer.Model
{
    public class VehicleServer : Inventory
    {
        public string Numberplate { get; set; }

        public Vehicle Handle { get; set; }

        public override int GetInventorySize()
        {
            return 100;
        }
    }
}
