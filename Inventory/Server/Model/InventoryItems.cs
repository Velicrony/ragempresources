﻿using System.ComponentModel;
using InventoryServer.Attributes;

namespace InventoryServer.Model
{
    public enum InventoryItems
    {
        [Description("Bottle")]
        [Weight(1)]
        Bottle,

        [Description("Missing")]
        [Weight(1)]
        Missing
    }
}
