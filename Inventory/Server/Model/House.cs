﻿namespace InventoryServer.Model
{
    public class House : Inventory
    {
        public int Id { get; set; }

        public override int GetInventorySize()
        {
            return 500;
        }
    }
}
