﻿namespace InventoryServer.Model
{
    public class CollectSpot : Inventory
    {
        public int Id { get; set; }

        public override int GetInventorySize()
        {
            return 80;
        }
    }
}
