﻿using GTANetworkAPI;

namespace InventoryServer.Model
{
    public class Player : Inventory
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public int TempId { get; set; }

        public Client Client { get; set; }

        public int HouseId { get; set; } = -1;

        public int CollectSpotId { get; set; } = -1;

        public override int GetInventorySize()
        {
            return 60;
        }
    }
}
